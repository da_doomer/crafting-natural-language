# crafting-natural-language

*Natural programming* is a programming paradigm where programs (known as
*natural programs*) are written primarily in natural language (for example,
English) and become executable by recursively augmenting each natural language
instruction with a sequence of more granular sub-instructions (also natural
language) until every instruction can be grounded in a conventional programming
language.

## Live demo

A [live demo](https://natural-programs.gitlab.io/crafting-natural-language/) is available.

## Development instructions

### Requirements

- [Node.js](https://en.wikipedia.org/wiki/Node.js)

### Interactive app

Using the development server requires the use of Chrome-based browsers because
of shenanigans on Vite's dealing with web workers in dev mode.

```bash
npm install
npm run dev
```

You can also build the website and use any browser you want:

```bash
npm install
npm run build
npm run preview
```

### Scripts

Create snapshot with complete solution library (will take a while):

```bash
npm install
node --experimental-specifier-resolution=node --loader ts-node/esm scripts/complete_solution_library.ts
```


## Deployment instructions

An example Gitlab CI script ([.gitlab-ci.yml](.gitlab-ci.yml)) is provided.

The database may have to be properly configured (e.g., set CORS headers and
access rules in Firebase).
