/**
 * Hyperparameter tuning utilities.
 */
import type { BeamSearchParameters } from "./session_snapshot";
import { Chance } from "chance";
import type { ProposeParameters } from "natural-programs/library/proposers";
import type { BeamParameters } from "./minecraft_app";

type Parameters = {
	beam_search_parameters: BeamSearchParameters,
	propose_parameters: ProposeParameters,
	beam_parameters: BeamParameters,
	max_queue_size: number,
};

/**
 * Perturb the given hyperparameters.
 */
function perturb(parameters: Parameters, seed: number): Parameters {
	const chance = new Chance(seed);

	// Perturb beam search parameters
	const p = parameters.beam_search_parameters;
	const new_sorting_noise_scale = p.sorting_noise_scale+chance.normal()*0.01;
	const new_max_top_level_sequence_length = p.max_top_level_sequence_length+chance.pickone([-1, 1, 0]);
	const new_library_beam_width = p.library_beam_width+chance.pickone([-1, 1, 0]);
	const new_propose_beam_width = p.propose_beam_width+chance.pickone([-1, 1, 0]);
	const new_p = {
		library_beam_width: Math.min(45, Math.max(1, new_library_beam_width)),
		propose_beam_width: Math.min(20, Math.max(1, new_propose_beam_width)),
		search_budget: p.search_budget,
		temperature: p.temperature, // There is no sampling
		sorting_noise_scale: Math.min(0.1, Math.max(0, new_sorting_noise_scale)),
		max_top_level_sequence_length: Math.min(10, Math.max(3, new_max_top_level_sequence_length)),
	};

	// Perturb propose parameters
	const pp = parameters.propose_parameters;
	const new_recency_weight = Math.min(1.0, Math.max(0.0, pp.recency_weight+chance.normal()*0.01));
	const new_pp = {
		recency_weight: new_recency_weight,
		frequency_weight: 1.0-new_recency_weight,
	}

	return {
		beam_search_parameters: new_p,
		propose_parameters: new_pp,
		beam_parameters: parameters.beam_parameters,
		max_queue_size: parameters.max_queue_size,
	};
}

export {
	perturb
};

export type {
	Parameters,
};
