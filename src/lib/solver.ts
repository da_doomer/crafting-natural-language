/**
 * Helper functions for calling solvers.
 **/
import type { SynthesisProblem } from "./minecraft_app";
import type { ProgramState } from "./minecraft_app";
import type { NaturalProcedureSketch } from "natural-programs/types";
import type { SolverResult } from "natural-programs/types";
import type { Library } from "natural-programs/library/library";
import { values } from "natural-programs/library/library";
import type { SolverError } from "natural-programs/types";
import type { Result } from "./workers";
import { solve_with_np } from "./workers";
import { solve_with_synthesis } from "./workers";
import { get_llm_completions } from "./openai";
import { get_llm_suggestion_sketches } from "natural-programs/library/llm_solver";
import { get_sorted_by_similarity } from "nlp/semantic_search";
import { string_sim_timeout } from "./minecraft_app";


enum SourceSolver {
	NP = "Natural Programming",
	LLMNP = "Natural Programming on LLM suggestions with empty beam",
	LLMDS = "Direct Synthesis on LLM suggestions with empty beam",
	DS = "Direct Synthesis",
	DP = "Direct Programming",
};


type SourcedSolverResult<T> = SolverResult<T> & {
	source_solver: SourceSolver
};

type SourcedResult = (SourcedSolverResult<ProgramState>|SolverError<ProgramState>);


// Intuition: we can discover L^3 with other solvers
const min_prompt_sketch_length = 4;


// Intuition: we want descriptive commands in prompt only
const min_prompt_sketch_name_length = 10;


/**
 * Helper function to get the latest `n` procedures in the library.
 */
function get_recent_procedure_sketches(
		library: Library,
		n: number
		): NaturalProcedureSketch[] {
	const recent_sketches: NaturalProcedureSketch[] = [];
	const library_sketches = [...library.added_sketches];
	library_sketches.reverse();

	const keys = new Set<string>();
	for (const sketch of library_sketches) {
		if (recent_sketches.length === n)
			break;
		if (typeof sketch === 'string')
			continue;
		if (!("steps" in sketch))
			continue;
		if (sketch.steps.length < min_prompt_sketch_length)
			continue;
		if (sketch.signature.name === undefined || sketch.signature.name.length < min_prompt_sketch_name_length)
			continue;
		const key = JSON.stringify(sketch);
		if (!(keys.has(key))) {
			keys.add(key);
			recent_sketches.push(sketch);
		}
	}
	return recent_sketches;
}


/**
 * Return the `n` closest procedure sketches in the library to the query
 * under semantic similarity.
 */
async function get_relevant_procedure_sketches(
		query: string,
		library: Library,
		n: number
		): Promise<NaturalProcedureSketch[]> {
	// Find sketch procedures names
	const sketch_names: string[] = [];
	for (const sketch of library.added_sketches) {
		if (typeof sketch === 'string')
			continue;
		if (!("steps" in sketch))
			continue;
		if (sketch.steps.length < min_prompt_sketch_length)
			continue;
		if (sketch.signature.name === undefined || sketch.signature.name.length < min_prompt_sketch_name_length)
			continue;
		const sketch_name = sketch.signature.name;
		if (sketch_name === undefined)
			continue;
		sketch_names.push(sketch_name);
	}

	// Sort sketches names
	const sorted_sketch_names = await get_sorted_by_similarity(
		sketch_names,
		query,
		string_sim_timeout
	);

	// Add sketches in order of similarity until we get to the desired number of
	// sketches
	const relevant_sketches: NaturalProcedureSketch[] = [];
	const relevant_sketches_keys = new Set<string>();
	for (const name of sorted_sketch_names) {
		for (const sketch of library.added_sketches) {
			if (typeof sketch === 'string')
				continue;
			if (!("steps" in sketch))
				continue;
			const sketch_name = sketch.signature.name;
			if (sketch_name !== name)
				continue;
			const key = JSON.stringify(sketch);
			if (!(relevant_sketches_keys.has(key))) {
				relevant_sketches_keys.add(key);
				relevant_sketches.push(sketch);
			}
			if (relevant_sketches.length >= n)
				return relevant_sketches;
		}
	}

	return relevant_sketches;
}


function get_post_conditions(library: Library): string[] {
	const post_conditions = [];
	for (const item of values(library)) {
		if (typeof item === 'string')
			continue;
		const post_condition = item.post_condition;
		if (post_condition === undefined)
			continue;
		post_conditions.push(post_condition);
	}
	return post_conditions;
}

/**
 * Solve the synthesis problem with the LLM natural programming solver in a
 * different thread.
 *
 * No exhaustive cross-product exploration, only the sketches returned from the
 * NP are tried to be solved (the beam is substituted with an empty array).
 */
async function solve_with_llm(
		synthesis_problem: SynthesisProblem,
		cancel_promise: Promise<void>|null,
		solver: SourceSolver,
		): Promise<Result> {
	const start_date = Date.now();
	if (solver !== SourceSolver.NP && solver !== SourceSolver.DS)
		throw "Unsupported solver!";

	console.log({submitted: synthesis_problem});

	// Create another NP worker to try LLM suggestions in parallel
	let break_llm_loop = false
	if (cancel_promise !== null)
		cancel_promise.then(() => { break_llm_loop = true; });

	const np = synthesis_problem.np;
	if (typeof np === 'string' || ("steps" in np)) {
		throw "Only signatures can be interpreted by this solver!";
	}

	// NP is signature, we can ask LLM for suggestions.
	const signature = np;

	// Identify primitives
	const primitives: string[] = [];
	for (const item of synthesis_problem.library.items.values()) {
		if (typeof item === 'string')
			primitives.push(item);
	}

	// Select procedures in library for prompt
	// TODO: selection should be smarter.
	const prompt_sketch_n = 5; // TODO: Parametrize number of prompt sketches
	const sketch_name = signature.name;
	const prompt_procedures = [
		...get_recent_procedure_sketches(
			synthesis_problem.library,
			prompt_sketch_n,
		),
	];
	if (sketch_name !== undefined)
		prompt_procedures.push(
		...await get_relevant_procedure_sketches(
			sketch_name,
			synthesis_problem.library,
			prompt_sketch_n,
		)
	);

	const suggestion_n = 15; // TODO: Parametrize number of suggestions
	const post_conditions = get_post_conditions(synthesis_problem.library);

	// Ask for suggestion sketches
	console.log("Requesting suggestions from LLM");
	const suggestions = get_llm_suggestion_sketches(
		signature,
		prompt_procedures,
		primitives,
		post_conditions,
		get_llm_completions,
		suggestion_n,
	);
	console.log("About to iterate suggestions!");

	// Keep track of total dequeue and milliseconds spent in solver
	let dequeue_n = 0;
	for await (const suggestion of suggestions) {
		console.log({suggestion: suggestion});
		const elapsed_time = Date.now() - start_date;
		if (elapsed_time > synthesis_problem.timeout_ms) break;
		if (break_llm_loop) break;

		// Craft a new synthesis problem with the suggestion (and
		// empty beam to avoid cross-product)
		const suggested_synthesis_problem = {
			...synthesis_problem,
			np: suggestion,
			timeout_ms: Math.min(3000, synthesis_problem.timeout_ms-elapsed_time),
		};
		// Attempt to solve the suggested problem
		const result = 
			(solver === SourceSolver.NP) ?
			await solve_with_np(
				suggested_synthesis_problem,
				cancel_promise,
			) : 
			await solve_with_synthesis(
				suggested_synthesis_problem,
				cancel_promise,
			);

		// Update stats
		dequeue_n = dequeue_n + result.dequeue_n;

		// If successful, return and reinitialize NP llm worker
		if ("error" in result) {
			console.log({
				event: "LLM suggestion failed",
				suggested_synthesis_problem: suggested_synthesis_problem,
				result: result,
			});
		} else {
			console.log({
				event: "LLM suggestion successful",
				suggested_synthesis_problem: suggested_synthesis_problem
			});
			return {
				...result,
				dequeue_n: dequeue_n,
				milliseconds: Date.now() - start_date,
			};
		}
	}
	return {
		error: "All LLM suggestions failed!",
		dequeue_n: dequeue_n,
		first_execution_error: null,
		milliseconds: Date.now() - start_date,
	};
}

/**
 * Append the source to a given solver result.
 */
function get_sourced_result<T>(
		result: SolverResult<T>,
		source: SourceSolver
		): SourcedSolverResult<T> {
	return {
		...result,
		source_solver: source,
	};
}

type SolverConfig = {
	np_synthesis_problem: SynthesisProblem|undefined,
	llmnp_synthesis_problem: SynthesisProblem|undefined,
	llmds_synthesis_problem: SynthesisProblem|undefined,
	ds_synthesis_problem: SynthesisProblem|undefined,
}

/**
 * Solve the synthesis problem with the LLM natural programming solver in a
 * different thread.
 */
async function solve_with_multisolver(
		solver_config: SolverConfig,
		cancel_promise: Promise<void>|null,
		): Promise<SourcedResult> {
	console.log({submitted: solver_config});

	const solver_promises: Promise<SourcedResult>[] = [];

	// Keep track if a solver is successful to avoid querying LLM unnecessarily
	let success_result: SourcedSolverResult<ProgramState>|null = null;

	// Start synthesis solver if requested
	if (solver_config.ds_synthesis_problem !== undefined) {
		const ds_promise = solve_with_synthesis(
			solver_config.ds_synthesis_problem,
			cancel_promise
		).then(function(result) {
			if (!("error" in result)) {
				const sourced_result = get_sourced_result(result, SourceSolver.DS);
				success_result = sourced_result;
				return sourced_result;
			}
			return result;
		});
		solver_promises.push(ds_promise);
	}

	// Start NP solver if requested
	if (solver_config.np_synthesis_problem !== undefined) {
		const np_promise = solve_with_np(
			solver_config.np_synthesis_problem,
			cancel_promise
		).then(function(result) {
			if (!("error" in result)) {
				const sourced_result = get_sourced_result(result, SourceSolver.NP);
				success_result = sourced_result;
				return sourced_result;
			}
			return result;
		});
		solver_promises.push(np_promise);
	}

	// Start LLM solver if requested only after 500ms if the other solvers
	// were not successful
	if (solver_config.llmnp_synthesis_problem !== undefined) {
		const llmnp_synthesis_problem = solver_config.llmnp_synthesis_problem;
		const llm_promise: Promise<SourcedResult> = new Promise(r => setTimeout(r, 500))
			.then(function() {
				if (success_result !== null)
					return success_result;
				return solve_with_llm(
					llmnp_synthesis_problem,
					cancel_promise,
					SourceSolver.NP,
				).then(function(result) {
					if (!("error" in result)) {
						const sourced_result = get_sourced_result(result, SourceSolver.LLMNP)
						success_result = sourced_result;
						return sourced_result;
					}
					return result;
				});
			});
		solver_promises.push(llm_promise);
	}

	// Start LLM solver if requested only after 500ms if the other solvers
	// were not successful
	if (solver_config.llmnp_synthesis_problem !== undefined) {
		const llmnp_synthesis_problem = solver_config.llmnp_synthesis_problem;
		const llm_promise: Promise<SourcedResult> = new Promise(r => setTimeout(r, 500))
			.then(function() {
				if (success_result !== null)
					return success_result;
				return solve_with_llm(
					llmnp_synthesis_problem,
					cancel_promise,
					SourceSolver.DS,
				).then(function(result) {
					if (!("error" in result)) {
						const sourced_result = get_sourced_result(result, SourceSolver.LLMDS)
						success_result = sourced_result;
						return sourced_result;
					}
					return result;
				});
			});
		solver_promises.push(llm_promise);
	}

	return new Promise(function(resolve, reject) {
		if (cancel_promise !== null)
			cancel_promise.then(()=> reject());

		const new_promises = [];
		let dequeue_n = 0;
		const start_time = Date.now();
		for (const promise of solver_promises) {
			const new_promise = promise.then(function(result) {
				dequeue_n = dequeue_n + result.dequeue_n;
				if (!("error" in result))
					return result;
				throw "error";
			}).catch(function(r) {
				console.error(r);
				throw r;
			});
			new_promises.push(new_promise);
		}
		Promise
			.any(new_promises)
			.then(e=>resolve(e))
			.catch(() => { resolve({
					error: "All solvers failed",
					dequeue_n: dequeue_n,
					first_execution_error: null,
					milliseconds: Date.now()-start_time
				});
			});
	});
}

export {
	solve_with_multisolver,
	SourceSolver,
};

export type {
	SourcedResult,
	SourcedSolverResult,
};
