import { get_active_chain_node, get_new_replay_id } from "./firebase";
import { get_chain } from "./firebase";
import { get_new_chain_node_id } from "./firebase";
import { get_session } from "./firebase";
import { get_snapshot } from "./firebase";
import { write_snapshot } from "./firebase";
import { get_new_snapshot_id } from "./firebase";
import { InventorySource } from "./firebase";
import { get_random_goal } from "$lib/goal";
import { get_goal_seed } from "$lib/goal";
import type { BeamSearchParameters } from "./session_snapshot";
import type { SessionType } from "./session_snapshot";
import type { Item } from "minecraft/items";
import type { ChainNode } from "./firebase";
import type { Library } from "natural-programs/library/library";
import type { BeamParameters, Goal } from "./minecraft_app";
import type { ProposeParameters } from "natural-programs/library/proposers";
import { write_session } from "./firebase";


type ChainInitPromise = Promise<{
	snapshot_id: string,
	snapshot_url: string,
	item_csv: string,
	mine_csv: string,
	session_type: SessionType,
	library: Library,
	inventory: Item[],
	beam_search_parameters: BeamSearchParameters,
	propose_parameters: ProposeParameters,
	beam_parameters: BeamParameters,
	goal: Goal | null,
	session_id: string,
	user_id: string,
	seed: string,
	session_timeout_ms: number|null,
	sat_item_bonus_usd: number|null,
	chain_node: ChainNode,
	starting_sat_items_names: string[],
	max_queue_size: number,
	replay_id: string,
}>;


async function new_snapshot() {
	const new_snapshot_id = await get_new_snapshot_id();
	const snapshot_url = new URL(window.location.href);
	snapshot_url.searchParams.set("start_snapshot_id", new_snapshot_id);
	return [new_snapshot_id, snapshot_url.toString()];
}


async function get_new_chain_snapshot_data(
		chain_id: string,
		icon_directory: string,
		session_id: string,
		user_id: string,
		): ChainInitPromise {
	// Create a new snapshot and a new session
	const [new_snapshot_id, snapshot_url] = await new_snapshot();
	const [start_snapshot_id, _] = await new_snapshot();

	// Load base snapshot of the chain
	const chain = await get_chain(chain_id);
	const base_snapshot = await get_snapshot(
		chain.initial_snapshot_id,
		icon_directory,
	);

	// Load snapshot of active node in the chain, if it exists
	const active_node = await get_active_chain_node(chain_id);

	// Use library from active node if it exists
	const library = (active_node === undefined) ?
		base_snapshot.library :
		(await get_snapshot(
			 (await get_session(active_node.node.session_id)
		).active_snapshot_id, icon_directory)).library;

	// If not first node, use active node ID as seed, otherwise
	// use chain ID.
	const height = (active_node === undefined) ?
		0 : active_node.height;
	const seed = get_goal_seed(chain.chain_seed, height);

	// Generate goal
	const goal = 
		(chain.goal_parameters !== undefined) ?
		get_random_goal(
			base_snapshot.item_csv,
			base_snapshot.mine_csv,
			seed,
			chain.goal_parameters,
		) : null;

	// Create inventory
	const active_snapshot = 
		(active_node === undefined) ?
		base_snapshot :
		await get_snapshot(
			(await get_session(active_node.node.session_id)).active_snapshot_id,
			icon_directory
		);
	const inventory =
		(chain.inventory_source === InventorySource.BaseInventory) ?
		base_snapshot.inventory :
		active_snapshot.inventory;
		

	// Create new snapshot
	const snapshot = {
		...base_snapshot,
		library: library,
		goal: goal,
		seed: seed,
		inventory: inventory,
	};
	await write_snapshot(new_snapshot_id, snapshot);

	// Store a new chain node
	const node_id = await get_new_chain_node_id();
	let chain_node: ChainNode = {
		id: node_id,
		chain_id: chain_id,
		session_id: session_id,
	}
	if (goal !== null)
		chain_node = {...chain_node, goal: goal.map(i=>i.name)};
	if (active_node !== undefined)
		chain_node = {...chain_node, previous_node_id: active_node.node.id};

	// Generate a new replay ID
	const replay_id = await get_new_replay_id();

	await write_session(
		session_id,
		{
			user_id: user_id,
			start_snapshot_id: start_snapshot_id,
			active_snapshot_id: new_snapshot_id,
			replay_id: replay_id,
		}
	);


	return {
		snapshot_id: new_snapshot_id,
		snapshot_url: snapshot_url,
		item_csv: snapshot.item_csv,
		mine_csv: snapshot.mine_csv,
		session_type: snapshot.session_type,
		library: snapshot.library,
		inventory: snapshot.inventory,
		beam_search_parameters: snapshot.beam_search_parameters,
		propose_parameters: snapshot.propose_parameters,
		beam_parameters: snapshot.beam_parameters,
		session_id: session_id,
		user_id: user_id,
		seed: seed,
		goal: goal,
		chain_node: chain_node,
		session_timeout_ms: chain.session_timeout_ms,
		sat_item_bonus_usd: chain.sat_item_bonus_usd,
		starting_sat_items_names: [],
		max_queue_size: snapshot.max_queue_size,
		replay_id: replay_id,
	};
}

export {
	get_new_chain_snapshot_data
};
