/**
 * Minecraft app utility functions.
 */
import type { CallstackContext } from "natural-programs/types";
import type { NaturalProcedure } from "natural-programs/types";
import type { NaturalProgram } from "natural-programs/types";
import type { NaturalProgramSketch } from "natural-programs/types";
import type { NaturalSignature } from "natural-programs/types";
import { add_recursive_batch, get_recency_score, new_library, values, type ItemID } from "natural-programs/library/library";
import type { Library } from "natural-programs/library/library";
import type { ProgramState } from "minecraft/semantics";
import type { CraftableItem, CraftingWorld } from "minecraft/items";
import type { Item } from "minecraft/items";
import type { MinecraftPrimitive } from "minecraft/grammar";
import type { MinecraftHierarchicalPrimitive } from "minecraft/grammar";
import type { DropdownInfo } from "./ide/blocks";
import type { DropdownOption } from "./ide/blocks";
import type Blockly from 'blockly';
import type { BeamSearchParameters } from "natural-programs/library/fringe_solver";
import type { ChainNode } from "./firebase";
import { add_recursive } from "natural-programs/library/library";
import { get } from "natural-programs/library/library";
import { get_concrete_implementations } from "natural-programs/library/library";
import { get_sequences } from "natural-programs/library/library";
import { get_k_nearest } from "natural-programs/library/library";
import { assign_id } from "natural-programs/library/library";
import { keys } from "natural-programs/library/library";
import { execute } from "minecraft/semantics";
import { is_mineable } from "minecraft/items";
import { naturalize_simple_primitive } from "minecraft/solutions";
import { naturalize_pick } from "minecraft/solutions";
import { naturalize_mine } from "minecraft/solutions";
import { get_constraint_as_string } from "minecraft/constraints";
import { MinecraftSimplePrimitive } from "minecraft/grammar";
import { BlockType } from "./ide/blocks";
import { function_definition_default_text } from "./ide/blocks";
import { icon_size } from "./ide/blocks";
import { NONE_DROPDOWN_OPTION } from "./ide/blocks";
import { primitive_as_block_info } from "./ide/blocks";
import { signature_default_text } from "./ide/blocks";
import { linearly_normalized, logsumexp } from "natural-programs/sampling";
import { sorted_distribution } from "natural-programs/sampling";
import { get_embeddings, get_similarities } from "nlp";
import { get_unrolled_implementation } from "natural-programs/unrolling";
import type Timer from "easytimer.js";
import type { ProposeParameters } from "natural-programs/library/proposers";
import type { SourcedSolverResult } from "./solver";

const string_sim_timeout = 2000;
const ui_solver_timeout_ms = 30000;

/**
 * Encapsulates information needed to interact with the interpreter using
 * strings as primitives.
 */
type MinecraftAPI = {
	primitives_map: Map<string, MinecraftPrimitive>,
};

type Goal = Item[];

/**
 * Build an API with the given world.
 */
function get_api<T extends Item>(world: CraftingWorld<T>): MinecraftAPI {
	// Primitives
	const primitives_map = new Map<string, MinecraftPrimitive>();
	const items = [...world.name_to_item.values()];
	for(const item of items) {
		const name = naturalize_pick(item);
		primitives_map.set(name, {item: item});
	}
	for (const item of items)
	if (is_mineable(item, world)) {
		const name = naturalize_mine(item);
		primitives_map.set(name, {mined_item: item});
	}
	for (const primitive of Object.values(MinecraftSimplePrimitive)) {
		const name = naturalize_simple_primitive(primitive);
		primitives_map.set(name, primitive);
	}
	return {
		primitives_map: primitives_map
	};
}

/**
 * Helper function to parses a primitive string into a structure that can be
 * understood by the interpreter.
 */
function primitive_parser(
		primitive: string,
		api: MinecraftAPI
		): MinecraftHierarchicalPrimitive {
	if(!(api.primitives_map.has(primitive)))
		throw 'Cannot parse "' + primitive + '"into original DSL!';
	return api.primitives_map.get(primitive) as MinecraftHierarchicalPrimitive;
}

/**
 * Returns the state after executing the given program, updating the UI.
 */
function concrete_interpreter(
		primitive: string, state: ProgramState, api: MinecraftAPI, world: CraftingWorld<Item>
		): ProgramState {
	return execute(primitive_parser(primitive, api), state, world);
}


type IDEToolboxInfo = [
	Blockly.utils.toolbox.ToolboxInfo,
	Map<string, ()=>Blockly.utils.toolbox.BlockInfo[]>,
	(illustration: string)=>string,
	(illustration: string)=>string|null,
	DropdownOption[],
	DropdownOption[],
	DropdownOption[],
]

/**
 * Return the item for use in a Blockly drop-down menu.
 */
function item_as_dropdown_info(item: Item): DropdownInfo {
	return {
		src: item.icon_src,
		width: icon_size*2,
		height: icon_size*2,
		alt: item.name,
	};
}

/**
 * Return a dynamic Blockly toolbox for the minecraft app and custom callbacks,
 * attached to the given library.
 */
function get_direct_programming_toolbox(
		world: CraftingWorld<Item>,
		signatures_button_callback: string,
		extra_buttons: {text: string, callback_key: string}[],
		): IDEToolboxInfo  {
	function primitive_map(option: string): string {
		return option;
	}

	function illustration_post_condition_map(option: string): string|null {
		if (option === NONE_DROPDOWN_OPTION) return null;
		return option;
	}

	const toolbox = {
		kind: "flyoutToolbox",
		contents: [
			...(extra_buttons.map(b=> {return {
				kind: "button",
				text: b.text,
				callbackKey: b.callback_key,
			}})),
			{
				kind: "label",
				text: "Teach",
			},
			{
				kind: "block",
				type: BlockType.SimpleFunctionDefinition,
			},
			{
				kind: "label",
				text: "Do",
			},
			{
				kind: "block",
				type: BlockType.DropdownPrimitive,
				fields: {
					NAME: "input",
				},
			},
			primitive_as_block_info(
				"craft",
				naturalize_simple_primitive(MinecraftSimplePrimitive.Craft),
			),
			{
				kind: "button",
				text: "Browse library",
				"web-class": "fake-block",
				callbackKey: signatures_button_callback,
			},
		],
	};

	const none_option: DropdownOption = ["none", NONE_DROPDOWN_OPTION];

	const signature_dropdown_options = [...world.name_to_item.values()].map(function(item) {
		const post = get_constraint_as_string({inventory: [item]});
		const option: DropdownOption = [item_as_dropdown_info(item), post];
		return option;
	});
	signature_dropdown_options.reverse();
	signature_dropdown_options.push(none_option);
	signature_dropdown_options.reverse();
	const function_definition_dropdown_options = signature_dropdown_options;

	const primitive_dropdown_options = [...world.name_to_item.values()].map(function(item) {
		const primitive = naturalize_pick(item);
		const option: DropdownOption = [item_as_dropdown_info(item), primitive];
		return option;
	});

	const callbacks = new Map<string, ()=>Blockly.utils.toolbox.BlockInfo>([]);
	return [
		toolbox,
		callbacks,
		primitive_map,
		illustration_post_condition_map,
		signature_dropdown_options,
		primitive_dropdown_options,
		function_definition_dropdown_options,
	];
}


/**
 * Return a dynamic Blockly toolbox for the minecraft app and custom callbacks,
 * attached to the given library. This toolbox only includes blocks strictly
 * necessary for natural programming (no library browsing).
 */
function get_natural_toolbox(
		world: CraftingWorld<Item>,
		extra_buttons: {text: string, callback_key: string}[],
		): [
		Blockly.utils.toolbox.ToolboxInfo,
		Map<string, ()=>Blockly.utils.toolbox.BlockInfo[]>,
		(illustration: string)=>string,
		(illustration: string)=>string|null,
		DropdownOption[],
		DropdownOption[],
		DropdownOption[],
		] {
	function primitive_map(option: string): string {
		return option;
	}

	function illustration_post_condition_map(option: string): string|null {
		if (option === NONE_DROPDOWN_OPTION) return null;
		return option;
	}

	const toolbox = {
		kind: "flyoutToolbox",
		contents: [
			...(extra_buttons.map(b=> {return {
				kind: "button",
				text: b.text,
				callbackKey: b.callback_key,
			}})),
			{
				kind: "block",
				type: BlockType.Signature,
			},
			// Primitives were disabled because users
			// were getting confused.
			/*
			{
				kind: "block",
				type: BlockType.DropdownPrimitive,
				fields: {
					NAME: "input",
				},
			},
			primitive_as_block_info(
				"craft",
				naturalize_simple_primitive(MinecraftSimplePrimitive.Craft),
			),
			*/
		]
	};

	const none_option: DropdownOption = ["none", NONE_DROPDOWN_OPTION];

	const signature_dropdown_options = [...world.name_to_item.values()].map(function(item) {
		const post = get_constraint_as_string({inventory: [item]});
		const option: DropdownOption = [item_as_dropdown_info(item), post];
		return option;
	});
	signature_dropdown_options.unshift(none_option);
	const function_definition_dropdown_options = signature_dropdown_options;

	const primitive_dropdown_options = [...world.name_to_item.values()].map(function(item) {
		const primitive = naturalize_pick(item);
		const option: DropdownOption = [item_as_dropdown_info(item), primitive];
		return option;
	});

	const callbacks = new Map<string, ()=>Blockly.utils.toolbox.BlockInfo>([]);
	return [
		toolbox,
		callbacks,
		primitive_map,
		illustration_post_condition_map,
		signature_dropdown_options,
		primitive_dropdown_options,
		function_definition_dropdown_options,
	];
}

/**
 * In-lines some recursion calls of the form f->[a,b,f]. More complicated
 * recursion patterns are not detected.
 */
function with_inlined_simple_recursion(np: NaturalProgramSketch): NaturalProgramSketch {
	if (typeof np === "string")
		return np;
	if (!("steps" in np))
		return np;

	// In-line recursion at this level until no recursion is detected.
	{
		let detected_recursion = false;
		const sig_key = JSON.stringify(np.signature);
		const new_steps = [];
		for (const step of np.steps) {
			const step_is_recursion = (
				typeof step !== "string" && ("steps" in step) && JSON.stringify(step.signature) === sig_key
			);
			if (step_is_recursion) {
				new_steps.push(...step.steps)
				detected_recursion = true;
			} else
				new_steps.push(step);
		}
		if (detected_recursion)
			return with_inlined_simple_recursion({
				signature: np.signature,
				steps: new_steps,
			});
	}

	// After recursion at this level is no longer detected, recursively in-line
	// recursive calls at each step
	const new_steps = [];
	for (const step of np.steps) {
		new_steps.push(with_inlined_simple_recursion(step));
	}
	return {
		signature: np.signature,
		steps: new_steps,
	};
}

/**
 * Add the program to the library, pre-processing the program if needed.
 */
async function add_to_library(
		np: NaturalProgram,
		library: Library,
		string_sim_timeout: number,
		) {
	const clean_np = with_inlined_simple_recursion(np);
	await add_recursive(library, clean_np, string_sim_timeout);
	return;
}

/**
 * Returns whether the goal is satisfied in the current state.
 */
function is_goal_satisfied(goal: Goal, state: ProgramState): boolean {
	for (const item of goal) {
		const owned_n = state.inventory.filter(i=>i.name===item.name).length;
		const goal_n = goal.filter(i=>i.name===item.name).length;
		if (owned_n < goal_n)
			return false;
	}
	return true;
}

/**
 * Inspect if the program satisfies minimum coding standards. If not,
 * throw an exception.
 */
function get_code_standards_error(sketch: NaturalProgramSketch): null|string {
	if (typeof sketch === "string") // Primitives are always ok.
		return null;

	// Signature case.
	if (!("steps" in sketch)) {
		const forbidden_names = [
			"",
			signature_default_text,
			function_definition_default_text,
		];
		if (sketch.name === undefined || forbidden_names.includes(sketch.name))
			return "Signatures with empty name are disallowed!";
		return null;
	}

	// Procedure case
	if (sketch.steps.length === 0)
		return "Function definitions with no steps are disallowed!";
	for (const step of sketch.steps) {
		const error = get_code_standards_error(step);
		if (error !== null)
			return error;
	}
	return get_code_standards_error(sketch.signature);
}

type BeamParameters = {
	recency_weight: number,
	language_weight: number,
	post_condition_weight: number,
}


/**
 * Return an augmented name for the sketch using the library.
 */
function get_augmented_name(query_signature: NaturalSignature): string {
	return (query_signature.name === undefined) ? "" : query_signature.name;
}


/**
 * Return the list of most-relevant signatures from the library.
 */
async function get_np_beam(
		np: NaturalProgramSketch,
		library: Library,
		beam_width: number,
		parameters: BeamParameters,
		): Promise<([NaturalSignature|string, number])[]> {
	let query_name: undefined|string = undefined;
	const query_signature = (typeof np === "string") ?
		undefined : ("signature" in np) ? np.signature : np;

		// If something similar has been solved before, find a list of
		// relevant primitive 'input X' commands.
	if (query_signature !== undefined) {
		query_name = get_augmented_name(query_signature);
		console.log({event: "Preprocessed sketch description", original_np: np, beam_query: query_name});
	}

	// If a name is available, score library according to language in signature
	const beam_ids: [ItemID, number][]  = (query_name === undefined) ?
		keys(library).map(i=>[i, 0.9]) :
		await get_k_nearest(
			library,
			query_name,
			null,
			string_sim_timeout
			);

	// Always put the last two signatures that are in the library
	// This is a small inductive bias for the synthesizer to include .
	const last_signatures = library
		.recently_used_ids
		.map(id=>get(library, id))
		.filter(np=>typeof np !== "string" && np !== undefined)
		.slice(-3)
		.map(np=>JSON.stringify(np));
	console.log({last_signatures: last_signatures})

	// Pretend scores are probability distribution and transform to log space
	const beam_ = sorted_distribution(logsumexp(beam_ids.map(
		function([id, language_score]): [NaturalSignature|string, number] {
			const item = get(library, id);
			if (item === undefined)
				throw 'Cannot find item! Did library change?';

			// Always include craft command
			if (item === naturalize_simple_primitive(MinecraftSimplePrimitive.Craft))
				return [item, 1.0];

			// Never include clear command
			if (item === naturalize_simple_primitive(MinecraftSimplePrimitive.Clear))
				return [item, 0.0];

			// Try to always include the last signatures
			if (typeof item !== "string" && last_signatures.includes(JSON.stringify(item)))
					return [item, 1.0];

			// If post-condition matches post-condition of query sketch, give it
			// maximum score
			if (typeof item !== "string"
			    && item.post_condition !== undefined
			    && query_signature !== undefined
			    && query_signature.post_condition === item.post_condition)
				return [item, 1.0];

			const post_condition_score = (typeof item !== "string"
			    && item.post_condition !== undefined
			    && query_signature !== undefined
			    && query_signature.post_condition === item.post_condition) ?
				1.0 : 0.0;
			const recency_score = (typeof item !== "string") ?
				get_recency_score(library, item) : 0.0;
			const score = language_score*parameters.language_weight
				+ recency_score*parameters.recency_weight
				+ post_condition_score*parameters.post_condition_weight;
			return [item, score];
		}
	)));
	const sorted_primitives: [string, number][] = [];
	const post_conditions = new Set<string>();
	const sorted_signatures: [NaturalSignature, number][] = [];
	for (const [np, score] of beam_) {
		if (typeof np === "string") {
			sorted_primitives.push([np, score]);
		} else {
			// Only include in beam one signature for each post-condition
			if (np.post_condition !== undefined && post_conditions.has(np.post_condition))
				continue;
			if (np.post_condition !== undefined)
				post_conditions.add(np.post_condition);
			sorted_signatures.push([np, score]);
		}
	}

	// Form beam
	return [
			...sorted_primitives,
			...sorted_signatures,
		].slice(0, beam_width);
}


/**
 * Return the list of most-relevant signatures from the library.
 */
async function get_concrete_beam(
		np: NaturalProgramSketch,
		library: Library,
		beam_width: number,
		parameters: BeamParameters,
		): Promise<([string|NaturalProcedure, number])[]> {
	let query_name: undefined|string = undefined;
	const query_signature = (typeof np === "string") ?
		undefined : ("signature" in np) ? np.signature : np;

		// If something similar has been solved before, find a list of
		// relevant primitive 'input X' commands.
	if (query_signature !== undefined) {
		query_name = get_augmented_name(query_signature);
		console.log({event: "Preprocessed sketch description", original_np: np, beam_query: query_name});
	}

	// If a name is available, sort library according to language in signature
	const beam_ids: [ItemID, number][]  = (query_name === undefined) ?
		keys(library).map(i=>[i, 0.9]) :
		await get_k_nearest(
			library,
			query_name,
			null,
			string_sim_timeout
			);

	// Pretend scores are probability distribution and transform to log space
	const beam_ = sorted_distribution(logsumexp(linearly_normalized(beam_ids.flatMap(
		function([id, language_score]): [NaturalProcedure|string, number][] {
			const item = get(library, id);
			if (item === undefined)
				throw 'Cannot find item! Did library change?';

			const post_condition_score = (typeof item !== "string"
			    && item.post_condition !== undefined
			    && query_signature !== undefined
			    && query_signature.post_condition === item.post_condition) ?
				1.0 : 0.0;
			const recency_score = (typeof item !== "string") ?
				get_recency_score(library, item) : 0.0;
			const score = language_score*parameters.language_weight
				+ recency_score*parameters.recency_weight
				+ post_condition_score*parameters.post_condition_weight;
			if (typeof item === "string")
				return [[item, score]];
			return get_concrete_implementations(library, item).map(np=>[np, score]);
		}
	))));
	const sorted_primitives: [string, number][] = [];
	const sorted_procedures: [NaturalProcedure, number][] = [];
	const added_implementations = new Set<string>();
	for (const [np, score] of beam_) {
		const implementation_key = JSON.stringify(get_unrolled_implementation(np));
		if (added_implementations.has(implementation_key))
			continue;
		else
			added_implementations.add(implementation_key);
		if (typeof np === "string") {
			sorted_primitives.push([np, score]);
		} else {
			sorted_procedures.push([np, score]);
		}
	}

	// Form beam
	return [
			...sorted_primitives,
			...sorted_procedures,
		].slice(0, beam_width);
}

/**
 * Return the average of the given numbers.
 */
function average(ns: number[]): number {
	if (ns.length === 0)
		throw "Cannot compute average of empty list!";
	let s = 0;
	for (const n of ns) s = s+n;
	return s/ns.length;
}

/**
 * Return the post condition of the closest signature in the library and its
 * similarity to the query, or undefined if no such post-condition exists.
 */
async function get_closest_post_condition(query: string, library: Library): Promise<[string, number]|null> {
	// Build a map from "post condition" to "queries associated with post condition
	const post_condition_queries = new Map<string, string[]>();
	for (const item of values(library)) {
		if (typeof item === "string"
		     || item.post_condition === undefined
		     || item.name === undefined)
			continue;
		const item_post_condition = item.post_condition;
		const item_query = item.name;
		const maybe_queries = post_condition_queries.get(item_post_condition);
		const queries = (maybe_queries === undefined) ? [] : maybe_queries;
		queries.push(item_query);
		if (maybe_queries === undefined)
			post_condition_queries.set(item_post_condition, queries);
	}

	// Cache all embeddings
	await get_embeddings([...post_condition_queries.keys()].flat(), string_sim_timeout);

	// Compute the mean of similarities between the query and post condition
	// queries
	let best_similarity = null;
	let best_post_condition = null;
	for (const [post_condition, queries] of post_condition_queries.entries()) {
		const mean_similarity = average(await get_similarities(query, queries, string_sim_timeout));
		if (best_similarity === null || mean_similarity > best_similarity) {
			best_similarity = mean_similarity;
			best_post_condition = post_condition;
		}
	}

	if (best_post_condition === null || best_similarity === null) return null;
	return [best_post_condition, best_similarity];
}

/**
 * Return the item with the most similar name to the query, and it's similarity
 * to the query.
 */
async function get_closest_item(query: string, world: CraftingWorld<Item>): Promise<[Item, number]> {
	const items = [...world.name_to_item.entries()];
	const similarities = await get_similarities(query, items.map(i=>i[0]), string_sim_timeout);
	const scored_items: [Item, number][] = items.map((d, i) => [d[1], similarities[i]]);
	scored_items.sort((i1, i2) => i2[1]-i1[1]);
	return scored_items[0];
}

/**
 * Pre-process sketch for search.
 */
async function preprocess_np_for_search(
		np: NaturalProgramSketch,
		library: Library,
		): Promise<NaturalProgramSketch> {
	if (typeof np === "string")
		return np;
	if ("steps" in np)
		return np;
	if (np.post_condition !== undefined || np.name === undefined)
		return np;

	// If np is signature and post-condition is not given, assign it one
	const post_condition = await get_closest_post_condition(np.name, library);

	if (post_condition !== null)
		return { ...np, post_condition: post_condition[0] };
	return np;
}

type SynthesisProblem = {
	np: NaturalProgramSketch,
	state: ProgramState,
	beam: ([NaturalProgramSketch, number])[],
	parameters: BeamSearchParameters,
	propose_parameters: ProposeParameters,
	world: CraftingWorld<Item>,
	api: MinecraftAPI,
	library: Library,
	timeout_ms: number,
	max_queue_size: number,
	fringe_seed: NaturalProgramSketch[],
};

/**
 * Return a URL that can be used to start a session with a copy of the given snapshot ID.
 */
function get_snapshot_url(snapshot_id: string, base: string) {
		const snapshot_url = new URL(base+"/configure", window.location.origin);
		snapshot_url.searchParams.set("start_snapshot_id", snapshot_id);
		return snapshot_url;
}


/**
 * Return a signature for a program that crafts the given item.
 */
function get_signature(item: Item): NaturalSignature {
	const constraint = {
		inventory: [item],
	};
	return {
		name: item.name,
		post_condition: get_constraint_as_string(constraint),
	}
}


/**
 * Return a top-level program that builds the given item in any state (assuming
 * sub-program sketches can be solved.
 */
function get_top_level_program(item: CraftableItem, items: Item[]): NaturalProgramSketch {
	// Craft all craftable items
	const craftable_prerequisites = [];
	const placed_items = [];
	for (const prerequisite_name of item.recipe) {
		const prerequisite = items.filter(i=>i.name===prerequisite_name).at(0);
		if (prerequisite === undefined)
			throw "Item in recipe '" + prerequisite_name + "' not found in world!";
		placed_items.push(prerequisite);
		if ("recipe" in prerequisite)
			craftable_prerequisites.push(prerequisite);
	}

	const steps = [
		...craftable_prerequisites.map(get_signature),
		...placed_items.map(i=>naturalize_pick(i)),
		naturalize_simple_primitive(MinecraftSimplePrimitive.Craft),
	];
	return {
		signature: get_signature(item),
		steps: steps,
	};
}


/**
 * Create the perfect library for the given world.
 */
async function get_perfect_np_library(
		items: Item[],
		string_sim_timeout: number
		): Promise<Library> {
	// Get a program for each craftable item
	const programs = [];
	for (const item of items) {
		if ("recipe" in item)
			programs.push(get_top_level_program(item, items));
	}

	// Build the library
	const library = new_library();
	await add_recursive_batch(library, programs, string_sim_timeout);
	return library;
}


/**
 * Return true if any item was consumed between the two states. That is,
 * return true if and only if the count of an item is strictly lower in the
 * second state.
 */
function is_item_consumed(s1: ProgramState, s2: ProgramState) {
	for (const name of new Set<string>(s1.inventory.map(i=>i.name))) {
		const s1_count = s1.inventory.filter(i=>i.name === name).length;
		const s2_count = s2.inventory.filter(i=>i.name === name).length;
		if (s2_count < s1_count)
			return true;
	}
	return false;
}

/**
 * Return remaining time in the session.
 */
function get_remaining_ms(
		session_timeout_ms: number,
		session_timer: Timer,
		show_tutorial_stopwatch: Timer,
		show_signature_execution_error_stopwatch: Timer,
		): number {
	return session_timeout_ms
		- session_timer.getTotalTimeValues().secondTenths*100
		+ show_tutorial_stopwatch.getTotalTimeValues().secondTenths*100
		+ show_signature_execution_error_stopwatch.getTotalTimeValues().secondTenths*100;
}

/**
 * Helper function to recursively remove all post-condition from the given
 * sketch.
 */
function remove_post_conditions(np: NaturalProgramSketch): NaturalProgramSketch {
	// Primitive case
	if (typeof np === "string")
		return np;

	// Signature case
	if (!("steps" in np))
		if (np.name === undefined) return {};
		else return { name: np.name };

	// Procedure case
	const signature = (np.signature.name === undefined) ? {} : {name: np.signature.name};
	return {
		signature: signature,
		steps: np.steps.map(remove_post_conditions),
	}
}

/**
 * Helper function to determine if a root signature is in a library.
 */
function is_root_signature_in_library(
		np: NaturalProcedure,
		library: Library,
	) {
	// Return whether there is expansion data for this signature
	return get_sequences(library, assign_id(library, np.signature)).length !== 0;
}

// Defintion of a list of buttons in the Blockly's left bar
type BlocklyButtons = {text: string, callback_key: string}[];

// Function that wraps a SketchSolver in the app.
type AppSketchSolver = (
		np: NaturalProgramSketch,
		s: ProgramState,
		l: Library,
		message: string,
		notify_if_error: boolean,
		force_cancel_ms: null|number,
		fringe_seed: NaturalProgramSketch[],
	)=>Promise<SourcedSolverResult<ProgramState>>

/**
 * Return all trajectories in the list of NPs. Assumes `nps` is a list from
 * least recent to most recent.
 *
 * If `nps = [n1, n2, ..., nn]` then this function returns
 * `[ [nn], [nn-1, nn], [nn-2, nn-1, nn], ..., [n1, ..., nn]]`.
 */
function get_recent_trajectories<T>(nps: T[]): T[][] {
	const trajectories = [];
	for (let i = 0; i < nps.length; i++)
	for (let j = i+1; j <= nps.length; j++) {
		const slice = nps.slice(i, j);
		trajectories.push(slice);
	}
	return trajectories;
}

/**
 * Get current fringe seed for the given sketch.
 */
function get_fringe_seed(np: NaturalProgramSketch, last_programs: NaturalProgramSketch[]): NaturalProgramSketch[] {
	// Filter list of programs to unrepeated programs
	const unrepeated_last_results = [];
	for (const result of last_programs) {
		const previous_result = unrepeated_last_results.at(-1);
		if (previous_result === undefined || JSON.stringify(previous_result) !== JSON.stringify(result))
			unrepeated_last_results.push(result)
	}

	// Seed is composed of sequences of programs from two sources:
	// - sequence of last unrepeated programs
	// - sequence of last programs
	// This covers some scenarios common in the crafting workflow.
	const most_recent_actions = last_programs.slice(-5);
	const most_recent_unrepeated_actions = unrepeated_last_results.slice(-5);
	const recent_trajectories = [
		...get_recent_trajectories(most_recent_unrepeated_actions),
		...get_recent_trajectories(most_recent_actions)
	];
	const fringe_seed = (typeof np === "string") ? [] : recent_trajectories.map(function(steps): NaturalProgramSketch {
		return {
			signature: ("signature" in np) ? np.signature : np,
			steps: steps,
		};
	}).map(with_inlined_simple_recursion);
	return fringe_seed;
}

export {
	concrete_interpreter,
	primitive_parser,
	string_sim_timeout,
	get_direct_programming_toolbox,
	get_natural_toolbox,
	get_api,
	add_to_library,
	is_goal_satisfied,
	get_code_standards_error,
	item_as_dropdown_info,
	get_np_beam,
	get_concrete_beam,
	preprocess_np_for_search,
	ui_solver_timeout_ms,
	get_closest_post_condition,
	get_closest_item,
	get_snapshot_url,
	get_perfect_np_library,
	is_item_consumed,
	get_remaining_ms,
	remove_post_conditions,
	is_root_signature_in_library,
	get_recent_trajectories,
	with_inlined_simple_recursion,
	get_fringe_seed,
	get_signature,
};

export type {
	CallstackContext,
	MinecraftAPI,
	CraftingWorld,
	ProgramState,
	IDEToolboxInfo,
	Goal,
	SynthesisProblem,
	BeamParameters,
	BlocklyButtons,
	AppSketchSolver,
	ChainNode,
};
