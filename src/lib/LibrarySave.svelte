<script lang="ts">
	/**
	 * Function to load sessions with a saving logic according to the given
	* parameters.
	 */
	import MinecraftApp from "./MinecraftApp.svelte";
	import Modal from "./Modal.svelte";
	import { get_fringe_seed, type Goal } from "./minecraft_app";
	import type { CraftingWorld } from "./minecraft_app";
	import type { ProgramState } from "./minecraft_app";
	import type { Item } from "minecraft/items";
	import type { NaturalProgramSketch, NaturalSignature } from "natural-programs/types";
	import type { AppSketchSolver } from "./minecraft_app";
	import type { Library } from "natural-programs/library/library";
	import type { BeamSearchParameters } from "./session_snapshot";
	import type Blockly from 'blockly';
	import type { ProposeParameters } from "natural-programs/library/proposers";
	import type { Timer } from "easytimer.js";
	import type { ChainNode } from "./firebase";
	import { is_item_consumed, type BeamParameters, type MinecraftAPI } from "./minecraft_app";
	import { item_as_dropdown_info } from "./minecraft_app";
	import { signature_as_block_info, type Outcome } from "./ide/blocks";
	import { BlockType, simple_signature_as_block_info } from "./ide/blocks";
	import { get_constraint_as_string } from "minecraft/constraints";
	import { string_sim_timeout } from "./minecraft_app";
	import { add_to_library } from "./minecraft_app";
	import { SessionType } from "./session_snapshot";
	import { is_root_signature_in_library } from "./minecraft_app";
	import { base } from '$app/paths';
	import { ui_solver_timeout_ms } from "./minecraft_app";

	const execution_modal_generalizing_message = "Your command was successful! The robot is now improving its performance with the new data.";

	export let solver: AppSketchSolver;
	export let state: ProgramState;
	export let library: Library;
	export let world: CraftingWorld<Item>;
	export let show_debug_menu: boolean;
	export let item_csv: string;
	export let mine_csv: string;
	export let session_type: SessionType;
	export let beam_search_parameters: BeamSearchParameters;
	export let propose_parameters: ProposeParameters;
	export let snapshot_id: string;
	export let snapshot_url: string;
	export let session_id: string;
	export let user_id: string;
	export let seed: string;
	export let goal: Goal|null;
	export let chain_node: ChainNode|null;
	export let tutorial_video_urls: {title: string, path: string}[];
	export let api: MinecraftAPI;
	export let beam_parameters: BeamParameters;
	export let session_timeout_ms: number|null;
	export let sat_item_bonus_usd: number|null;
	export let starting_sat_items_names: string[];
	export let solver_stopwatch: Timer;
	export let max_queue_size: number;
	export let replay_id: string|null;
	export let show_library_stopwatch: Timer;
	export let show_signature_execution_error_stopwatch: Timer;

	const items_in_scope = [...world.name_to_item.values()];
	const post_conditions: string[] = [];
	for(const item of items_in_scope) {
		const constraint = get_constraint_as_string({
			inventory: [item],
		});
		post_conditions.push(constraint);
	}
	post_conditions.sort();

	// Saved modal
	let saved_modal_message: ({
		workspace: Blockly.WorkspaceSvg,
		signature: NaturalSignature
		add_to_workspace: ()=>void,
		}|null) = null;

	// Overwrite modals
	let confirm_overwrite_np_name = "";
	let confirm_overwrite_callback: (null|((user_confirmed: boolean)=>Promise<void>)) = null;

	// Keep track of whether generalization has failed
	let avoid_generalizing_np: (null|NaturalProgramSketch) = null;

	/**
	 * Function to attempt saving the outcome of a successful search to the
	 * library.
	 * This function calls the solver and adds any successful results to the
	 * given list of last results.
	 */
	async function save_successful_execution(
			outcome: Outcome<ProgramState>,
			block: (Blockly.BlockSvg|null),
			library: Library,
			notify: boolean,
			): Promise<void> {
		// If session type is Direct Programming programs cannot be overwritten.
		if (session_type === SessionType.DirectProgramming) {
			const should_ask = (typeof outcome.program === "string") ?
				false :
				is_root_signature_in_library(outcome.program, library);
			if (should_ask) {
				confirm_overwrite_np_name = (typeof outcome.program === "string") ?
					outcome.program :
					(outcome.program.signature.name === undefined) ?
					"This program" :
					"'" + outcome.program.signature.name + "'";
				const user_confirmed = await (new Promise<boolean>((resolve, _) => {
					confirm_overwrite_callback = async (user_confirmed: boolean) => {
						confirm_overwrite_callback = null;
						resolve(user_confirmed);
					};
				}));
				if (!user_confirmed)
					return;
			}
		}

		// Reset generalization avoidance if appropriate
		if (JSON.stringify(outcome.sketch) !== JSON.stringify(avoid_generalizing_np))
			avoid_generalizing_np = null;

		// If primitive, simply record the execution to the library
		const np = outcome.program;
		if (typeof np === "string") {
			await add_to_library(outcome.program, library, string_sim_timeout);
			return;
		};

		// Extract signature
		const signature = ("steps" in np) ? np.signature : np;
		const post_condition_items = (signature.post_condition === undefined) ?
			[] : JSON.parse(signature.post_condition);

		// Do not add programs whose signature has more than one item
		if (post_condition_items.length > 1)
			throw "Only programs with at most one item in post condition supported!";
		const item = (post_condition_items.length > 0) ?
			post_condition_items[0] :
			null;

		// Add the program
		await add_to_library(outcome.program, library, string_sim_timeout);

		// Attempt to generalize program if session type is not direct programming
		if (session_type !== SessionType.DirectProgramming
		    && JSON.stringify(outcome.sketch) !== JSON.stringify(avoid_generalizing_np)) {
			// Execute it a couple of extra times to find implementations that
			// generalize
			let state = outcome.final_state;
			const generalization_start = Date.now();
			for (let i = 0; i < 22; i++) {
				const max_ms = ui_solver_timeout_ms - Date.now() - generalization_start;
				try {
					const sub_outcome = await solver(
						outcome.sketch,
						state,
						library,
						execution_modal_generalizing_message,
						false,
						max_ms,
						get_fringe_seed(outcome.sketch, library.added_sketches),
					);
					await add_to_library(
						sub_outcome.program,
						library,
						string_sim_timeout,
					);
					if (!(is_item_consumed(state, sub_outcome.final_state)))
						break;
					state = sub_outcome.final_state;
				} catch (err) {
					// Either timeout or user cancelled or synthesis failed
					avoid_generalizing_np = outcome.sketch;
					break;
				}
			}
		}

		if (notify && block !== null) {
			// Build callback for the "add to workspace" button
			function add_to_workspace() {
				const workspace = block.workspace;
				const info = (signature.post_condition === undefined || item === null) ?
					simple_signature_as_block_info(signature)
					: signature_as_block_info(
						signature,
						[[
							item_as_dropdown_info(item),
							signature.post_condition
						]],
						signature.post_condition,
						base,
					);
				const new_block = (signature.post_condition === undefined || item === null) ?
					workspace.newBlock(BlockType.SimpleSignature)
					: workspace.newBlock(BlockType.Signature);
				for (const [field, value] of Object.entries(info.fields)) {
					new_block.setFieldValue(value, field);

					// Make the blocks un-editable
					const block_field = new_block.getField(field);
					if (block_field !== null)
						block_field.setEnabled(false);
				}
				new_block.initSvg();
				new_block.moveBy(
					workspace.getMetrics().viewLeft/workspace.scale,
					workspace.getMetrics().viewTop/workspace.scale,
				);
				workspace.render();
			}

			saved_modal_message = {
				signature: np.signature,
				workspace: block.workspace,
				add_to_workspace: add_to_workspace,
			};
		}
	}
</script>

<MinecraftApp
	user_id={user_id}
	snapshot_id={snapshot_id}
	state={state}
	library={library}
	solver={solver}
	save_successful_execution={save_successful_execution}
	world={world}
	show_debug_menu={show_debug_menu}
	mine_csv={mine_csv}
	item_csv={item_csv}
	session_type={session_type}
	beam_search_parameters={beam_search_parameters}
	propose_parameters={propose_parameters}
	snapshot_url={snapshot_url}
	session_id={session_id}
	seed={seed}
	goal={goal}
	chain_node={chain_node}
	tutorial_video_urls={tutorial_video_urls}
	api={api}
	beam_parameters={beam_parameters}
	sat_item_bonus_usd={sat_item_bonus_usd}
	session_timeout_ms={session_timeout_ms}
	starting_sat_items_names={starting_sat_items_names}
	solver_stopwatch={solver_stopwatch}
	max_queue_size={max_queue_size}
	replay_id={replay_id}
	show_library_stopwatch={show_library_stopwatch}
	show_signature_execution_error_stopwatch={show_signature_execution_error_stopwatch}
	on:task_completed
/>

{#if saved_modal_message !== null}
	<Modal
			on:close="{() => {saved_modal_message=null;}}"
			close_message={"Great!"}
			>
		<h1 class="title">Robot learned something new!</h1>
		<div class="content">
			<p>Robot has learned a new {(saved_modal_message.signature.name === undefined) ? "skill" : "way to do '" + saved_modal_message.signature.name + "'"}!</p>
		</div>
		<button class="button" on:click={()=>{if (saved_modal_message !== null) saved_modal_message.add_to_workspace(); saved_modal_message = null;}}>
			Add to canvas
		</button>
	</Modal>
{/if}

{#if confirm_overwrite_callback !== null}
	<Modal
			show_close_button={false}
			>
		<h1 class="title">Tried to save, but...</h1>
		<div class="content">
			<p>{confirm_overwrite_np_name} already exists!</p>
		</div>
		<div>
			<button class="button" on:click={()=>{if (confirm_overwrite_callback !== null) confirm_overwrite_callback(false)}}>
				Cancel, I will use a different name
			</button>
			<!--
			<button class="button is-danger" on:click={()=>{if (confirm_overwrite_callback !== null) confirm_overwrite_callback(true)}}>
				Overwrite
			</button>
			-->
			<button class="button is-danger" disabled>
				Overwrite
			</button>
		</div>
	</Modal>
{/if}
