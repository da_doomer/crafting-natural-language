type SurveyResponse = {
	session_id: string,
	problems: string;
	age: number;
	gender: string;
	gender_other: string;
	communication_strategies: string;
	frustration: number;
	engagement: number;
	mental_demand: number;
	ease_of_use: number;
	hurry: number;
	self_success: number;
	effort: number;
	programming_experience: string;
}

/**
 * Return a verbose naturalization of the question key, or the key if there
 * is no such naturalization.
 */
function get_question(key: string): string {
	if (key === "age") return "Age";
	if (key === "gender") return "Gender";
	if (key === "mental_demand") return "How mentally demanding was this task?";
	if (key === "hurry") return "How hurried or rushed were you during the task?";
	if (key === "self_success") return "How successful would you rate yourself in accomplishing the task?";
	if (key === "gender") return "How hard did you have to work to accomplish your level of performance?";
	if (key === "frustration") return "How irritated or stressed were you while performing the task?";
	if (key === "engagement") return "How engaged were you while performing the task?";
	if (key === "ease_of_use") return "How easy was it to use the system?";
	if (key === "communication_strategies") return "What strategy did you use to solve the task with the robot?";
	if (key === "problems") return "Did you encounter any problems?";
	if (key == "effort") return "How hard did you have to work to accomplish your level of performance?";
	if (key == "programming_experience") return "What is your computer-programming experience?";
	return key;
}

export {
	get_question,
};

export type {
	SurveyResponse
};
