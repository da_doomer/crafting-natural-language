import type { SourceSolver } from "./solver";

type SessionEvent = {
	session_id: string,
	session_milliseconds: number 
};

type FailedExecution = SessionEvent & {
	kind: "failed_execution",
	np: string,
	error: string
	milliseconds: number, // execution length
	execution_snapshot_id: string,
};

type SuccessfulExecution = SessionEvent & {
	kind: "successful_execution",
	np: string,
	concrete_np: string,
	dequeue_n: number,
	milliseconds: number, // execution length
	execution_snapshot_id: string,
	source_solver?: SourceSolver,
};

type LibraryOpen = SessionEvent & {
	kind: "library_open",
};

type LibraryClose = SessionEvent & {
	kind: "library_close",
};

type RecipesOpen = SessionEvent & {
	kind: "recipes_open",
};

type RecipesClose = SessionEvent & {
	kind: "recipes_close",
};

type TutorialOpen = SessionEvent & {
	kind: "tutorial_open",
};

type TutorialClose = SessionEvent & {
	kind: "tutorial_close",
};

type BlocklyEvent = SessionEvent & {
	kind: "blockly_event",
	blockly_event_json: unknown,
};

type TaskCompletion = SessionEvent & {
	kind: "task_completed",
};

type Event = (
	SuccessfulExecution | FailedExecution
	| LibraryOpen | LibraryClose
	| RecipesOpen | RecipesClose
	| TutorialOpen | TutorialClose
	| BlocklyEvent
	| TaskCompletion
);

export type {
	SuccessfulExecution,
	FailedExecution,
	Event,
};
