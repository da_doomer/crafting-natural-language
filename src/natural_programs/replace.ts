/**
 * Module to replace a sub-program within a natural program sketch.
 **/
import type { NaturalProgramSketch } from "natural-programs/types";

/**
 * Helper function to replace a node in `np`, indexed by callstack indices
 * sequence `index`, with `new_tree`.
 */
function get_replaced(np: NaturalProgramSketch, index: number[], replacement: NaturalProgramSketch): NaturalProgramSketch {
	if (index.length === 0)
		return replacement;
	if (typeof np === "string" || !("steps" in np))
		throw 'Node index does not exist in NaturalProgramSketch';
	const new_steps = [];
	for (let i = 0; i < np.steps.length; i++) {
		const original_step = np.steps[i];
		const step = (i === index[0]) ?
			get_replaced(original_step, index.slice(1), replacement)
			:
			original_step;
		new_steps.push(step);
	}
	const new_tree = {
		signature: np.signature,
		steps: new_steps,
	};
	return new_tree;
}

export {
	get_replaced,
}
