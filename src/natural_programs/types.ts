/**
 * Type definitions for the "primitives are fundamentally different from
 * natural procedures" version.
 *
 * There is no restriction to search only "primitives", it is more
 * general and we can search over longer programs in the original DSL.
 */
/**
 * Without loss of generality we assume primitives are strings in a DSL.
 */
type Primitive = string;

/**
 * A natural signature may involve a natural language name and pre- and
 * post-conditions.
 */
type NaturalSignature = {
	name?: string,
	pre_condition?: string,
	post_condition?: string,
};

/**
 *
 * Natural procedure are recursive structures which contain
 * a sequence of instructions.
 */
type NaturalProcedure = {
	signature: NaturalSignature,
	steps: (Primitive|NaturalProcedure)[],
};

type NaturalProgram = (Primitive | NaturalProcedure);

/**
 *
 * Natural procedure sketches are recursive structures which (may) contain
 * a constraint, a name and a sequence of instructions.
 */
type NaturalProcedureSketch = {
	signature: NaturalSignature,
	steps: (NaturalSignature|Primitive|NaturalProcedureSketch)[],
};

type NaturalProgramSketch = (NaturalSignature|Primitive|NaturalProcedureSketch);

/**
 * A mapping from programs in a DSL and program states to program states.
 */
type ConcreteInterpreter<ProgramState> = (
		program: string, state: ProgramState,
	) => ProgramState;

/**
 * A mapping from programs in a DSL and program states to boolean values.
 */
type ConstraintInterpreter<ProgramState> = (
		constraing: string, old_state: ProgramState, new_state: ProgramState,
	) => boolean;


type Callstack = [NaturalSignature, number][];

/**
 * Contexts are provided to the user's propose function. They model
 * the stack trace of the natural program that is being executed.
 */
type CallstackContext<ProgramState> = {
	callstack: Callstack,
	state: ProgramState,
};

/**
 * A finite distribution is a list of objects and raw score pairs.
 */
type FiniteDistribution<T> = [T, number][];

/**
 * A mapping that takes a context and returns a distribution over a finite
 * list of natural programs. The distribution is encoded as pairs of
 * programs and raw scores. Scores will be normalized into a probability
 * distribution.
 */
type NaturalProposer<ProgramState> = (
		c: CallstackContext<ProgramState>,
		signature: NaturalSignature,
	) => FiniteDistribution<NaturalProgramSketch[]>;

/**
 * Solver results describe a particular concrete solution to a sketch.
 */
type SolverResult<ProgramState> = {
	program: NaturalProgram,
	final_state: ProgramState,
	dequeue_n: number,
	milliseconds: number,
};

/**
 * Generic Natural Sketch solver.
 */
type SketchSolver<ProgramState> = (
		np: NaturalProgramSketch,
		state: ProgramState,
		) => Promise<SolverResult<ProgramState>>;

type SolverState = {
	early_termination: {terminate: boolean},
	last_pause: {last_pause: number}
};

type UnknownFoundExecution<ProgramState> = {
	context: CallstackContext<ProgramState>,
	signature: NaturalSignature,
};

type ExecutionException = {
	execution_exception: string,
};

type PostConditionFailed = {
	failed_post_condition: string,
};

type ExecutionError<ProgramState> = {
	error: ExecutionException|PostConditionFailed,
	np: NaturalProgram|NaturalProgramSketch,
	state: ProgramState,
	context: CallstackContext<ProgramState>
};

type SuccessfulExecution<ProgramState> = {
	program: NaturalProgram,
	last_state: ProgramState,
};

type PartialExecutionState<ProgramState> = UnknownFoundExecution<ProgramState>
	| ExecutionError<ProgramState>
	| SuccessfulExecution<ProgramState>;

/**
 * NaturalProcedureSketch that has been partially executed.
 */
type AnnotatedNaturalProcedureSketch<ProgramState> = NaturalProcedureSketch & {
	last_fully_executed_step: {index: number, state: ProgramState},
	executed_steps: (NaturalProcedure|string)[],
};

type ProgramID = string;

type SolverError<ProgramState> = {
	error: string,
	dequeue_n: number,
	first_execution_error: ExecutionError<ProgramState>|null,
	milliseconds: number,
}

export type {
	NaturalProcedureSketch,
	NaturalProgramSketch,
	NaturalProcedure,
	NaturalProgram,
	ConcreteInterpreter,
	ConstraintInterpreter,
	NaturalProposer,
	CallstackContext,
	Primitive,
	FiniteDistribution,
	Callstack,
	SolverResult,
	NaturalSignature,
	SketchSolver,
	SolverState,
	UnknownFoundExecution,
	ExecutionError,
	SuccessfulExecution,
	PartialExecutionState,
	ProgramID,
	SolverError,
	ExecutionException,
	PostConditionFailed,
	AnnotatedNaturalProcedureSketch,
};
