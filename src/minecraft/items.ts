/**
 * Abstraction over Minecraft items and list of items.
 */

type PrimitiveItem = {
	is_tool: boolean,
	name: string,
	score: number,
	is_infinite: boolean,
	icon_src: string,
}

/**
 * A craftable item has a recipe. The recipe is a 3x3 grid of
 * id_mcs with -1 representing an empty space.  **/
type CraftableItem = PrimitiveItem & {
	recipe: string[],
}
/**
 * A breakable item has a remaining durability value.
 */
type BreakableItem = PrimitiveItem & {
	durability: number,
}

type Item = (PrimitiveItem|CraftableItem|BreakableItem);

/**
 * An object that encapsulates information in a crafting world.
 */
type CraftingWorld<T extends Item> = {
	// (tool_name|null)->(mined_item_name->yield_n)
	mining_yields: Map<(string|null), Map<string, number>>;
	// item_name -> item
	name_to_item: Map<string, T>;
}

/**
 * Return the items in the given world.
 */
function get_items<T extends Item>(world: CraftingWorld<T>): T[] {
	return [...world.name_to_item.values()];
}

/**
 * Return whether the given tool can mine the given item.
 */
function is_mineable<T extends Item>(item: Item, world: CraftingWorld<T>): boolean {
	const maybe_tools = [
		...world.name_to_item.values(),
		null
	];
	for (const maybe_tool of maybe_tools)
	if (can_mine(maybe_tool, item, world))
		return true;
	return false;
}


/**
 * Return whether the given tool can mine the given item.
 */
function can_mine<T extends Item>(
		tool: Item | null,
		item: Item,
		world: CraftingWorld<T>
		): boolean {
	return get_yield(tool, item, world) > 0;
}

/**
 * Return the number of items yielded by using the given tool to
 * mining the given item. Passing `null` as the item returns
 * the number of items yielded by mining the item by hand.
 */
function get_yield<T extends Item>(
		tool: Item | null,
		item: Item,
		world: CraftingWorld<T>
		): number {
	const tool_key = (tool === null) ? null : tool.name;
	const minable_yields = world.mining_yields.get(tool_key);
	if (minable_yields === undefined)
		return 0;
	const yield_n = minable_yields.get(item.name);
	if (yield_n === undefined) return 0;
	return yield_n;
}

/**
 * Return whether the item appears in any recipe in the world.
 */
function appears_in_recipe<T extends Item>(item: T, world: CraftingWorld<T>): boolean {
	for (const item2 of get_items(world))
		if ("recipe" in item2 && item2.recipe.includes(item.name))
			return true;
	return false;
}

/**
 * Return all items that are recursively necessary to craft the given item,
 * including the item itself.
 */
function get_items_in_crafting_subtree(
		item: Item,
		world: CraftingWorld<Item>,
		): Item[] {
	if (!("recipe" in item))
		return [item];

	const subtree_items = [];

	// Recursively get items in the crafting subtrees
	// of the children
	for (const item_name of item.recipe) {
		const child_item = world.name_to_item.get(item_name);
		if (child_item === undefined)
			throw "Item " + item_name + " is not in crafting world!";
		const items = get_items_in_crafting_subtree(
			child_item,
			world,
		);
		subtree_items.push(...items);
	}
	subtree_items.push(item);
	return subtree_items;
}

export type {
	Item,
	PrimitiveItem,
	CraftableItem,
	BreakableItem,
	CraftingWorld
};

export {
	can_mine,
	get_yield,
	is_mineable,
	appears_in_recipe,
	get_items_in_crafting_subtree,
	get_items,
};
