"""Draw the plots from the theory analysis."""
import matplotlib.pyplot as plt
import random
import csv

# make a sampler for a specific recipe
def get_item_recipe_map(csv_path):
    item_recipe_map = {}

    with open(csv_path, 'r') as csv_file:
        i = 0
        for row in csv.reader(csv_file):
            if i == 0:
                i += 1
                continue
            # get the item_id, which is actually the name as well
            item_id = row[0]
            # get the recipes which is the last 2 columns
            recipe2 = row[-1]
            recipe1 = row[-2]

            recipes = []
            for recipe in [recipe1, recipe2]:
                # split the recipe into a list
                recipe_list = recipe.split(',')
                # trim the spaces around the recipe items
                recipe_list = [item.strip() for item in recipe_list]
                # remove empty strings
                recipe_list = [item for item in recipe_list if item]
                recipes.append(recipe_list)

            # add the item_id and recipe to the dictionary
            item_recipe_map[item_id] = recipes

    return item_recipe_map


def make_random_world(recipe_map):
    world_recipe = {}
    for item, recipes in recipe_map.items():
        # check if the recipes is has empty lists in it i.e. []
        if [] in recipes:
            world_recipe[item] = recipes[0]+recipes[1]
        else:
            # pick one of the recipes randomly, then add it
            # to the world recipe
            world_recipe[item] = random.choice(recipes)
    return world_recipe


def np_prob_of_enough_data(
        current_generation_i: int,
        top_level_puzzle_solution_n: int,
        max_puzzle_height: int,
        ) -> float:
    l = top_level_puzzle_solution_n
    g = current_generation_i
    i = max_puzzle_height
    return (1 - (1 - 1/l)**(g-1))**(i)


def ds_prob_of_enough_data(
        current_generation_i: int,
        top_level_puzzle_solution_n: int,
        max_puzzle_height: int,
        ) -> float:
    l = top_level_puzzle_solution_n
    g = current_generation_i
    i = max_puzzle_height
    return 1 - (1 - (1/l)**i)**(g-1)


## Get average complexity
# load the recipe book
csv_path = 'recipe_v5.csv'
item_recipe_map = get_item_recipe_map(csv_path)


def get_item_complexity(item: str, world) -> int:
    children_complexities = [
        get_item_complexity(child, world)
        for child in world[item]
    ]
    return 1 + sum(children_complexities, start=0)

def get_average_item_complexity(item: str, sim_n: int) -> float:
    complexities = list()
    for _ in range(sim_n):
        recipe_map = get_item_recipe_map(csv_path)
        world = make_random_world(recipe_map)
        complexity = get_item_complexity(item, world)
        complexities.append(complexity)
    return sum(complexities)/len(complexities)

sim_n = 1000
item_complexities = [
    get_average_item_complexity('bed', sim_n=sim_n),
    get_average_item_complexity('cloth', sim_n=sim_n),
    get_average_item_complexity('bread', sim_n=sim_n),
    get_average_item_complexity('house', sim_n=sim_n),
    get_average_item_complexity('light_bulb', sim_n=sim_n),
    get_average_item_complexity('clock', sim_n=sim_n),
]
print(item_complexities)
# /Average complexity

top_level_puzzle_solution_n = 2
generation_n = 20

gens = list(range(1, generation_n))
y_np = list()
y_ds = list()
for g in gens:
    # NP:
    y_np_probs = list()
    for complexity in item_complexities:
        p = np_prob_of_enough_data(
                current_generation_i=g,
                top_level_puzzle_solution_n=top_level_puzzle_solution_n,
                max_puzzle_height=complexity,
            )
        y_np_probs.append(p)
    expected_np_items = sum(y_np_probs)
    y_np.append(expected_np_items)

    y_ds_probs = list()
    for complexity in [3,4,5,8,9,10]:
        p = ds_prob_of_enough_data(
                current_generation_i=g,
                top_level_puzzle_solution_n=top_level_puzzle_solution_n,
                max_puzzle_height=complexity,
            )
        y_ds_probs.append(p)
    expect_ds_items = sum(y_ds_probs)
    y_ds.append(expect_ds_items)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(gens, y_np, label="Expected NP items built")
ax.plot(gens, y_ds, label="Expected DS items built")
plt.legend()
output_svg = 'theory_plot3.svg'
plt.savefig(output_svg)
print(f'Wrote {output_svg}')
