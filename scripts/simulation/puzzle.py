from typing import NewType
import itertools
import random
import math


Primitive = int
Candidate = NewType('Candidate', tuple[Primitive, ...])
Solution = NewType('Solution', tuple[Primitive, ...])
PuzzleID = str
PuzzleSolution = Solution|tuple[PuzzleID, ...]
RecipeBook = NewType('RecipeBook', dict[PuzzleID, PuzzleSolution])
"""Keys range from easy puzzles to hard puzzles. No loops allowed."""
MetaRecipeBook = NewType('MetaRecipeBook', dict[PuzzleID, tuple[PuzzleSolution, ...]])
"""Keys range from easy puzzles to hard puzzles. No loops allowed."""


def get_solution(puzzle_id: PuzzleID, recipe_book: RecipeBook) -> Solution:
    """Return the oracle solution for the given puzzle."""
    solution = list[Primitive]()
    for element in recipe_book[puzzle_id]:
        if isinstance(element, Primitive):
            solution.append(element)
        else:
            solution.extend(get_solution(element, recipe_book))
    return Solution(tuple(solution))


def is_solution(candidate: Candidate, puzzle_id: PuzzleID, context: RecipeBook) -> bool:
    """Return true if the candidate matches the puzzle solution."""
    return candidate == get_solution(puzzle_id, context)


class SolverFailedError(Exception):
    pass


def get_random_recipe_book(meta_recipe_book: MetaRecipeBook) -> RecipeBook:
    """Return a randomly constructed recipe book."""
    recipe_book = {
        puzzle_id: random.choice(options)
        for puzzle_id, options in meta_recipe_book.items()
    }
    return RecipeBook(recipe_book)


def get_synthetic_meta_recipe_book(
        N: int,
        l: int,
        height: int,
        name_start: int = 0,
        ) -> tuple[MetaRecipeBook, int, set[Primitive]]:
    """Return a synthetic meta recipe book with the given properties. Each
    solution is a permutation of the puzzles of one-less height.

    - :param N is the size of the solutions of each puzzle. > 0
    - :param l is the number of solutions that each puzzle can take. > 0
    - :param height is the height of the meta recipe book (it will be a tree).
      Should be greater than zero.

    Returns the name of the top level puzzle as an integer.
    """
    if height == 0:
        return MetaRecipeBook(dict()), name_start, set()

    if height == 1:
        # Generate l solutions of length N of primitives.
        # The exact sequences do not matter as long as they are different.
        # We will use the first N natural numbers as primitives. We could
        # use any number n for which n! > l, though (we do permutations to
        # generate different sequences).
        primitives = list(range(name_start, name_start+N))
        psolutions: tuple[Solution, ...] = tuple(
            Solution(solution)
            for solution in itertools.islice(
                itertools.permutations(primitives, r=N),
                l,
            )
        )
        top_level_puzzle_id = str(name_start+N)
        meta_book = MetaRecipeBook({
            top_level_puzzle_id: psolutions
        })
        return meta_book, name_start+N, set(primitives)

    # Generate l top-level sub puzzles
    primitives = set()
    sub_puzzle_ids = list()
    meta_recipe_book = MetaRecipeBook(dict())
    for _ in range(max(l, math.factorial(max(l, N))+1)):
        sub_meta_recipe_book, sub_puzzle_id, sprimitives = get_synthetic_meta_recipe_book(
            N,
            l,
            height-1,
            name_start=name_start,
        )
        sub_puzzle_ids.append(sub_puzzle_id)
        name_start = sub_puzzle_id + 1
        meta_recipe_book |= sub_meta_recipe_book
        primitives |= sprimitives

    # Generate l solutions with the generated top level puzzles
    sub_puzzle_ids_str = [str(pid) for pid in sub_puzzle_ids]
    solutions: tuple[tuple[PuzzleID, ...], ...] = tuple(
        solution
        for solution in itertools.islice(
            itertools.permutations(sub_puzzle_ids_str, r=N),
            l,
        )
    )

    top_puzzle_id = str(name_start)
    meta_recipe_book[top_puzzle_id] = solutions
    return meta_recipe_book, name_start, primitives
