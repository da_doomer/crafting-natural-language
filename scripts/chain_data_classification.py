"""Extract the classified executions from the chain data obtained through the
download button in the aggregation plots page."""
import argparse
import json
from pathlib import Path

parser = argparse.ArgumentParser(
    description='Extract classified executions from the chain data.'
)
parser.add_argument(
    '--input_json',
    type=Path,
    help='Aggregation data in JSON format.'
)
parser.add_argument(
    '--output_json',
    type=Path,
    help='Output data in JSON format.'
)
args = parser.parse_args()

# Open JSON
with open(args.input_json) as fp:
    values = json.load(fp)

# Extract classified execution for each chain
classified_executions = dict()
for chain_data in values:
    chain_id = chain_data['chain_id']
    classified_executions[chain_id] = chain_data['classified_executions']

# Write classified executions
with open(args.output_json, 'wt') as fp:
    json.dump(classified_executions, fp, indent=2)
